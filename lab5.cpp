/************************
Michael Cole
mgcole
Lab 3
Lab Section 001
Hanjie Liu
************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/

/* 
*Integers j,k,l, and m are there to make sure the values of each suit only go from 2-14, the *loop iterates through all 52 Card structs in the deck, and then for each group of 14 cards for *each suit, each card is assigned a value and its suit in order* 
*/
	int j = 2, k = 2, l = 2, m = 2;
	Card deck[52];
	for(int i=0; i < 52; ++i){
		if(i < 13){
			deck[i].suit = SPADES;
			deck[i].value = j;
			++j;
		}
		else if((i >=13) && (i < 26)){
			deck[i].suit = HEARTS;
			deck[i].value = k;
			++k;
		}
		else if((i >=26) && (i < 39)){
			deck[i].suit = DIAMONDS;
			deck[i].value = l;
			++l;
		}
		else if((i >=39) && (i < 52)){
			deck[i].suit = CLUBS;
			deck[i].value = m;
			++m;
		}
	}

  /*After the deck is created and initialized we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	
	Card *firstCard = &deck[0];
	Card *pastLastCard = (firstCard + 52);
	
	random_shuffle(firstCard, pastLastCard, myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/

/*
*Array of struct type Card is created with 5 pieces of data, each being a card from the deck *that is now shuffled.
*/

	Card myHand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/

	Card *firstHand = myHand;
	Card *pastLastHand = (firstHand + 5);
	sort(firstHand, pastLastHand, suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

/*
*A for loop iterates through each of the Card structs in the Hand array, which is the user's *hand of 5 cards that are sorted. If the card's value is over 10, in which case it has a name, *the name of the card is fetched using the get_card_name function, and after that it prints of *and the suit of the card by using the get_suit_code function. All of this is formatted to print *on lines of width 10, right justified.
*/

	for(int j = 0; j < 5; ++j){
	   if(myHand[j].value > 10){		
		cout << right << setw(10) << get_card_name(myHand[j]) << " of " << get_suit_code(myHand[j]) << endl;
	   }
	   else{
		cout << right << setw(10) << myHand[j].value << " of " << get_suit_code(myHand[j]) << endl;
	   }
	}


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

/*
*Suit_order function compares two cards passed into it, if the suit on the left hand side is *less than the rhs, the cards are in order, and it returns true, if they're not, it returns *false, and if they're equal the values are compared, in which case if the lhs < rhs then it's *true and otherwise it's false. This function is used in the sorting algorithm.
*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit){
  	return true; 
  }
  else if(lhs.suit == rhs.suit){
	if(lhs.value < rhs.value){
	return true;
	}
	else if(lhs.value > rhs.value){
	return false;
	}
  }
  else if(lhs.suit > rhs.suit){
	return false;
  }
  else{
	return false;
  }
 //to prevent compiler warning
    return true;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

/*
*Similar to get_suit_code, the get_card_name function looks at the values of each card that's *over 10, (10 included as a control), and returns the proper card name, if nothing matches it *prints out Error.
*/
string get_card_name(Card& c) {
  switch (c.value) {
     case 10: return "10";
     case 11: return "Jack";
     case 12: return "Queen";
     case 13: return "King";
     case 14: return "Ace";
     default: return "Error";
  }
}
//Shuffles the deck of cards
void random_shuffle(Card *card1, Card *pastCard52, int myrandom)
{}
//Sorts the hand of cards
void sort(Card *card1, Card *pastCard5, bool suit_order)
{}
